$(document).ready(function () {
    var logo = $("#logo");
    logo.addClass("start" + " light");
    logo.removeClass("opacity");

    var textBottom = $("#text");

    var arrClass = [];

    textBottom.animate({bottom: '10px'}, 1000, function (){
        logo.removeClass("light");
        $("body").removeClass("fixed");
        textBottom.animate({bottom: '-10px'}, 600, function () {
            textBottom.animate({bottom: '0px'}, 600);
        });
    });

    function animate() {
        arrClass = logo.attr('class').split(' ');
        for(var i = 0; i < arrClass.length; i++){
            if('light' === arrClass[i]){
                logo.removeClass('light');
            } else {
                logo.addClass('light');
            }
        }
    }

    setInterval(animate, 1500);

});
