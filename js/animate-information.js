$(document).ready(function () {
    var docPos = $('#documents').offset().top;
    $(window).scroll(function () {
        if($(window).scrollTop() >= docPos - 500){
            $(".information").removeClass('opacity');
            $("#documents").animate({marginLeft: '0'}, 400);
            $("#photos").animate({marginRight: '0'}, 400);
        }
    });
});
